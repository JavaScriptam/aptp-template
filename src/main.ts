import { createApp } from "vue";
import { router } from "./router";
import installComponent from "./components/index";
import { createPinia } from "pinia";
import Antd from "ant-design-vue";
import App from "./App.vue";
import "./style.less";

const vueApp = createApp(App).use(router);
vueApp.use(Antd);

vueApp.use(createPinia());
vueApp.use(installComponent);
vueApp.mount("#app");
