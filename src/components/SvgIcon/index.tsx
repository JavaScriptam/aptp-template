import { defineComponent, computed } from "vue";
import iconLess from "./icon.module.less";
export default defineComponent({
  name: "SvgIcon",
  props: {
    prefix: {
      type: String,
      default: "icon",
    },
    className: {
      type: String,
      default: "svg-icon",
    },
    name: {
      type: String,
      required: true,
    },
    color: {
      type: String,
      default: "black",
    },
  },
  setup(props) {
    const { color, className } = props;
    const symbolId = computed(() => `#${props.prefix}-${props.name}`);
    return () => {
      return (
        <svg
          class={`${className} ${iconLess["emo-svg-SvgIcon"]}`}
          aria-hidden="true"
        >
          <use href={symbolId.value} fill={color} />
        </svg>
      );
    };
  },
});
