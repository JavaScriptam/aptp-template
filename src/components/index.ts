import { defineAsyncComponent } from "vue";

const modulesFiles = import.meta.glob("./*/*.tsx");
const pathList: string[] = [];
//遍历拿到所有的文件名称
for (const path in modulesFiles) {
  pathList.push(path);
}
//全局批量注册components下所有组件
export default {
  install(app?: any) {
    pathList.forEach((path) => {
      const component = modulesFiles[path];
      app.component(path.split("/")[1], defineAsyncComponent(component as any));
    });
  },
};
