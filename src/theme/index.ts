import { ConfigProvider } from "ant-design-vue";
import color from "./color.json";
ConfigProvider.config({
  theme: color,
});
