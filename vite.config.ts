import { defineConfig } from "vite";
import { resolve } from "path";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import { svgBuilder } from "./build/svgBuilder";

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: [
      {
        find: "@",
        replacement: resolve(__dirname, "src"),
      },
    ],
  },
  plugins: [vue(), vueJsx(), svgBuilder("./src/components/SvgIcon/svg/")],
  css: {
    // 指定传递给 CSS 预处理器的选项; 文件扩展名用作选项的键
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
});
